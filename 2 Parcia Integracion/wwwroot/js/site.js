﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

$(document).ready(function () {
    $("#Valor_Compra,#Fecha_Registro").change(function () {
        var fecha = new Date();
        var date = new Date($('#Fecha_Registro').val());

        var tiempo = fecha - date;
        var diff = tiempo / (1000 * 3600 * 24);
        var years = Math.floor(diff / 365.25);
        var valor = $('#Valor_Compra').val();
        valor = valor / 4;

        if (date === null || valor === null) {
            $('#Depreciacion_Acumulada').val(0);
        }
        else if (years < 1) {
            $('#Depreciacion_Acumulada').val(0);
        }
        else if (years <= 4) {
            $('#Depreciacion_Acumulada').val(valor * years);
            console.log(years);
            console.log(valor);
        }
        else {
            $('#Depreciacion_Acumulada').val(valor);
            console.log(years);
            console.log(valor);
        }
    });
});