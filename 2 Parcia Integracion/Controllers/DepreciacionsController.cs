﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using _2_Parcia_Integracion.Data;
using _2_Parcia_Integracion.Models;
using Microsoft.AspNetCore.Authorization;

namespace _2_Parcia_Integracion.Controllers
{
    [Authorize]
    public class DepreciacionsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public DepreciacionsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Depreciacions
        public async Task<IActionResult> Index()
        {
            return View(await _context.Depreciacion.ToListAsync());
        }

        // GET: Depreciacions/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var depreciacion = await _context.Depreciacion
                .FirstOrDefaultAsync(m => m.Id == id);
            if (depreciacion == null)
            {
                return NotFound();
            }

            return View(depreciacion);
        }

        // GET: Depreciacions/Create
        public IActionResult Create()
        {
            ViewData["Activo_FijoID"] = new SelectList(_context.Set<ActivosFijos>(), "Id", "Descripcion");
            return View();
        }

        // POST: Depreciacions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Anio_Proceso,Mes_Proceso,Activo_FijoID,Fecha_Proceso,Moto_Depreciado,Deprecion_Acumulada,Cuenta_Compra,Cuenta_Depreciacion")] Depreciacion depreciacion)
        {
            if (ModelState.IsValid)
            {
                _context.Add(depreciacion);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["Activo_FijoID"] = new SelectList(_context.Set<ActivosFijos>(), "Id", "Descripcion", depreciacion.Activo_FijoID);
            return View(depreciacion);
        }

        // GET: Depreciacions/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var depreciacion = await _context.Depreciacion.FindAsync(id);
            if (depreciacion == null)
            {
                return NotFound();
            }
            ViewData["Activo_FijoID"] = new SelectList(_context.Set<ActivosFijos>(), "Id", "Descripcion", depreciacion.Activo_FijoID);
            return View(depreciacion);
        }

        // POST: Depreciacions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Anio_Proceso,Mes_Proceso,Activo_FijoID,Fecha_Proceso,Moto_Depreciado,Deprecion_Acumulada,Cuenta_Compra,Cuenta_Depreciacion")] Depreciacion depreciacion)
        {
            if (id != depreciacion.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(depreciacion);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DepreciacionExists(depreciacion.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["Activo_FijoID"] = new SelectList(_context.Set<ActivosFijos>(), "Id", "Descripcion", depreciacion.Activo_FijoID);
            return View(depreciacion);
        }

        // GET: Depreciacions/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var depreciacion = await _context.Depreciacion
                .FirstOrDefaultAsync(m => m.Id == id);
            if (depreciacion == null)
            {
                return NotFound();
            }

            return View(depreciacion);
        }

        // POST: Depreciacions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var depreciacion = await _context.Depreciacion.FindAsync(id);
            _context.Depreciacion.Remove(depreciacion);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool DepreciacionExists(int id)
        {
            return _context.Depreciacion.Any(e => e.Id == id);
        }
    }
}
