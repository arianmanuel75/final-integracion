﻿namespace _2_Parcia_Integracion.Dtos
{
    public class AuxiliaryAccountDto
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public bool Active { get; set; }
    }
}