﻿namespace _2_Parcia_Integracion.Dtos
{
    public class CurrencyTypeDto
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public decimal LastExchangeRate { get; set; }
        public int Status { get; set; }
    }
}