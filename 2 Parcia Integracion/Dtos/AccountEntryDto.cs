﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace _2_Parcia_Integracion.Dtos
{
    public class AccountEntryDto
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public int AuxiliaryAccountId { get; set; }
        public AuxiliaryAccountDto AuxiliaryAccount { get; set; }
        public string Account { get; set; }
        public int MovementType { get; set; }
        public DateTime Created { get; set; }
        public string Period { get; set; }
        public int Amount { get; set; }
        public int CurrencyTypeId { get; set; }
        public CurrencyTypeDto CurrencyType { get; set; }
        public int Status { get; set; }
    }
}
