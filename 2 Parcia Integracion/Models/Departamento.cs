﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace _2_Parcia_Integracion.Models
{
    public class Departamento
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Este campo es necesario")]
        public string Descripcion { get; set; }

        public bool Estado { get; set; }

        public ICollection<ActivosFijos> ActivosFijos { get; set; }
        public ICollection<Empleado> Empleados { get; set; }
    }
}
