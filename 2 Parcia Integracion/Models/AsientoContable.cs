﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace _2_Parcia_Integracion.Models
{
    public enum TipoMovimiento
    {
        DB, CR
    }

    public class AsientoContable
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Este campo es requerido")]
        public string Descripcion { get; set; }

        [Required(ErrorMessage = "Este campo es requerido")]
        [Display(Name = "Tipo de inventario")]
        public int Tipo_Inventario_Id { get; set; }

        [Required(ErrorMessage = "Este campo es requerido")]
        [Display(Name = "Cuenta Contable")]
        public int Cuenta_Contable { get; set; }

        [Required(ErrorMessage = "Este campo es requerido")]
        [Display(Name = "Tipo de movimiento")]
        public TipoMovimiento Tipo_Movimiento { get; set; }

        [Required(ErrorMessage = "Este campo es requerido")]
        [Display(Name = "Fecha del asiento")]
        [DataType(DataType.Date)]
        public DateTime Fecha_Asiento { get; set; }

        [Required(ErrorMessage = "Este campo es requerido")]
        [Display(Name = "Monto del asiento")]
        [Range(0, int.MaxValue, ErrorMessage = "El Valor de no debe ser negativo")]
        [DataType(DataType.Currency)]
        public decimal Monto_Asiento { get; set; }

        public bool Estado { get; set; }
    }
}
