﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace _2_Parcia_Integracion.Models
{
    public class ActivosFijos
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Este campo es necesario")]
        public string Descripcion { get; set; }

        [Display(Name = "Departamento")]
        [Required(ErrorMessage = "Este campo es necesario")]
        public int DepartamentoID { get; set; }

        [Display(Name = "Tipo de activo fijo")]
        [Required(ErrorMessage = "Este campo es necesario")]
        public int Tipo_ActivoID { get; set; }

        [Display(Name = "Fecha de registro")]
        [Required(ErrorMessage = "Este campo es necesario")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime Fecha_Registro { get; set; }

        [Display(Name = "Valor de compra")]
        [Required(ErrorMessage = "Este campo es necesario")]
        [DataType(DataType.Currency)]
        [Range(1, int.MaxValue, ErrorMessage = "El Valor de compra debe ser mayor a 0")]
        public decimal Valor_Compra { get; set; }

        [Display(Name = "Deprecion acumulada")]
        [Required(ErrorMessage = "Este campo es necesario")]
        [Range(0, int.MaxValue, ErrorMessage = "El Valor de no debe ser negativo")]
        [DataType(DataType.Currency)]
        public decimal Depreciacion_Acumulada { get; set; }

        public virtual Departamento Departamento { get; set; }

        [Display(Name = "Tipo de activo fijo")]
        public virtual TipoDeActivo Tipo_Activo { get; set; }

        public ICollection<Depreciacion> Depreciacions { get; set; }
    }
}
