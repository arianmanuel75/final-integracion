﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using _2_Parcia_Integracion.Models.Validaciones;

namespace _2_Parcia_Integracion.Models
{
    public enum TipoPersona
    {
        Fisica, Juridica
    }
    
    public class Empleado
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Este campo es necesario")]
        public string Nombre { get; set; }

        [Required(ErrorMessage = "Este campo es necesario")]
        public string Apellido { get; set; }

        [Required(ErrorMessage = "Este campo es necesario")]
        
        [RegularExpression(@"\b\d{3}\-?\d{7}\-?\d{1}\b", ErrorMessage = "La cedula no es valida")]
        [ValidateCedula(ErrorMessage = "La cedula no es valida")]
        public string Cedula { get; set; }

        [Display(Name = "Departamento")]
        [Required(ErrorMessage = "Este campo es necesario")]
        public int DepartamentoID { get; set; }

        [Display(Name = "Tipo de presona")]
        [Required(ErrorMessage = "Este campo es necesario")]
        public TipoPersona Tipo_Persona { get; set; }

        [Display(Name = "Fecha de ingreso")]
        [Required(ErrorMessage = "Este campo es necesario")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime Fecha_Ingreso { get; set; }

        public bool Estado { get; set; }

        public virtual Departamento Departamento { get; set; }
    }
}
