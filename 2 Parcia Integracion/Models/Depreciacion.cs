﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace _2_Parcia_Integracion.Models
{
    public class Depreciacion
    {
        public int Id { get; set; }

        [Display(Name = "Activo fijo")]
        [Required(ErrorMessage = "Este campo es necesario")]
        public int Activo_FijoID { get; set; }

        [Display(Name = "Fecha del proceso")]
        [Required(ErrorMessage = "Este campo es necesario")]
        [DataType(DataType.Date)] 
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/yyyy}")]
        //public DateTime Fecha_Proceso { get { return DateTime.("{0}/{1}", Mes_Proceso, Anio_Proceso); } }
        public DateTime Fecha_Proceso { get; set; }

        [Display(Name = "Monto depreciado")]
        [Required(ErrorMessage = "Este campo es necesario")]
        [DataType(DataType.Currency)]
        public decimal Moto_Depreciado { get; set; }

        [Display(Name = "Deprecion acumulada")]
        [Required(ErrorMessage = "Este campo es necesario")]
        [DataType(DataType.Currency)]
        public decimal Deprecion_Acumulada { get; set; }

        [Display(Name = "Cuenta de compra")]
        [Required(ErrorMessage = "Este campo es necesario")]
        public int Cuenta_Compra { get; set; }

        [Display(Name = "Cuenta de depreciacion")]
        [Required(ErrorMessage = "Este campo es necesario")]
        public int Cuenta_Depreciacion { get; set; }

        public virtual ActivosFijos ActivosFijos { get; set; }
    }
}
