﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace _2_Parcia_Integracion.Data.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Departamento",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Descripcion = table.Column<string>(nullable: false),
                    Estado = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Departamento", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TipoDeActivo",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Descripcion = table.Column<string>(nullable: false),
                    Cuenta_Contable_Compra = table.Column<int>(nullable: false),
                    Cuenta_Contable_Depreciacion = table.Column<int>(nullable: false),
                    Estado = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TipoDeActivo", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Empleado",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Nombre = table.Column<string>(nullable: false),
                    Apellido = table.Column<string>(nullable: false),
                    Cedula = table.Column<string>(nullable: false),
                    DepartamentoID = table.Column<int>(nullable: false),
                    Tipo_Persona = table.Column<int>(nullable: false),
                    Fecha_Ingreso = table.Column<DateTime>(nullable: false),
                    Estado = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Empleado", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Empleado_Departamento_DepartamentoID",
                        column: x => x.DepartamentoID,
                        principalTable: "Departamento",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ActivosFijos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Descripcion = table.Column<string>(nullable: false),
                    DepartamentoID = table.Column<int>(nullable: false),
                    Tipo_ActivoID = table.Column<int>(nullable: false),
                    Fecha_Registro = table.Column<DateTime>(nullable: false),
                    Valor_Compra = table.Column<decimal>(nullable: false),
                    Depreciacion_Acumulada = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ActivosFijos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ActivosFijos_Departamento_DepartamentoID",
                        column: x => x.DepartamentoID,
                        principalTable: "Departamento",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ActivosFijos_TipoDeActivo_Tipo_ActivoID",
                        column: x => x.Tipo_ActivoID,
                        principalTable: "TipoDeActivo",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Depreciacion",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Anio_Proceso = table.Column<DateTime>(nullable: false),
                    Mes_Proceso = table.Column<DateTime>(nullable: false),
                    Activo_FijoID = table.Column<int>(nullable: false),
                    Fecha_Proceso = table.Column<DateTime>(nullable: false),
                    Moto_Depreciado = table.Column<decimal>(nullable: false),
                    Deprecion_Acumulada = table.Column<decimal>(nullable: false),
                    Cuenta_Compra = table.Column<int>(nullable: false),
                    Cuenta_Depreciacion = table.Column<int>(nullable: false),
                    ActivosFijosId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Depreciacion", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Depreciacion_ActivosFijos_ActivosFijosId",
                        column: x => x.ActivosFijosId,
                        principalTable: "ActivosFijos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ActivosFijos_DepartamentoID",
                table: "ActivosFijos",
                column: "DepartamentoID");

            migrationBuilder.CreateIndex(
                name: "IX_ActivosFijos_Tipo_ActivoID",
                table: "ActivosFijos",
                column: "Tipo_ActivoID");

            migrationBuilder.CreateIndex(
                name: "IX_Depreciacion_ActivosFijosId",
                table: "Depreciacion",
                column: "ActivosFijosId");

            migrationBuilder.CreateIndex(
                name: "IX_Empleado_DepartamentoID",
                table: "Empleado",
                column: "DepartamentoID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Depreciacion");

            migrationBuilder.DropTable(
                name: "Empleado");

            migrationBuilder.DropTable(
                name: "ActivosFijos");

            migrationBuilder.DropTable(
                name: "Departamento");

            migrationBuilder.DropTable(
                name: "TipoDeActivo");
        }
    }
}
