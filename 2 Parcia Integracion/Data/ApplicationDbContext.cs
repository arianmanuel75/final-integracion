﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using _2_Parcia_Integracion.Models;

namespace _2_Parcia_Integracion.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        public DbSet<_2_Parcia_Integracion.Models.ActivosFijos> ActivosFijos { get; set; }
        public DbSet<_2_Parcia_Integracion.Models.Departamento> Departamento { get; set; }
        public DbSet<_2_Parcia_Integracion.Models.Depreciacion> Depreciacion { get; set; }
        public DbSet<_2_Parcia_Integracion.Models.Empleado> Empleado { get; set; }
        public DbSet<_2_Parcia_Integracion.Models.TipoDeActivo> TipoDeActivo { get; set; }
    }
}
