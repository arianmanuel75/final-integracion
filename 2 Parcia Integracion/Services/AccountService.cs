﻿using _2_Parcia_Integracion.Dtos;
using _2_Parcia_Integracion.Models;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace _2_Parcia_Integracion.Services
{
    public class AccountService
    {
        private readonly HttpClient _httpClient;
        private IConfiguration _configuration;

        public AccountService(HttpClient httpClient, IConfiguration configuration)
        {
            _httpClient = httpClient;
            _configuration = configuration;
            _httpClient.BaseAddress = new Uri(_configuration.GetSection("ApiSettigns:RootUri").Value);
            _httpClient.DefaultRequestHeaders.Accept.Clear();
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<HttpStatusCode> PostEntry(ActivosFijos activosFijos)
        {
            List<AccountEntryDto> entriesDto = new List<AccountEntryDto>();

            var auxilary = GetAuxiliary(8);
            var currency = GetCurrencyType("Peso");

            var gasto = new AccountEntryDto
            {
                Id = 65,
                AuxiliaryAccountId = auxilary.Id,
                AuxiliaryAccount = auxilary,
                Amount = Convert.ToInt32(activosFijos.Valor_Compra),
                Created = DateTime.UtcNow.AddDays(-4),
                Account = "Gasto depreciación Activos Fijos",
                MovementType = 1,
                CurrencyType = currency,
                CurrencyTypeId = currency.Id,
                Description= $"Descripcion: Asiento de Activos Fijos correspondiente al periodo {DateTime.UtcNow.Year}-{DateTime.UtcNow.Month}",
                Period = $"{DateTime.UtcNow.Year}-{DateTime.UtcNow.Month}",
                Status = 0
            };


            var depreciacion = new AccountEntryDto
            {
                Id = 66,
                AuxiliaryAccountId = auxilary.Id,
                AuxiliaryAccount = auxilary,
                Amount = Convert.ToInt32(activosFijos.Valor_Compra),
                Created = DateTime.UtcNow.AddDays(-4),
                Account = "Depreciación Acumulada Activos Fijos",
                MovementType = 0,
                CurrencyType = currency,
                CurrencyTypeId = currency.Id,
                Description = $"Descripcion: Asiento de Activos Fijos correspondiente al periodo {DateTime.UtcNow.Year}-{DateTime.UtcNow.Month}",
                Period = $"{DateTime.UtcNow.Year}-{DateTime.UtcNow.Month}",
                Status = 0
            };

            var infoAsiento = new
            {
                AccountingEntryDebit = gasto,
                AccountingEntryCredit = depreciacion
            };

            string json = JsonConvert.SerializeObject(infoAsiento);
            var httpContent = new StringContent(json, Encoding.UTF8, "application/json");
            var httpResponse = await _httpClient.PostAsync("account-entry", httpContent);

            return httpResponse.StatusCode;
        }

        public async Task<HttpStatusCode> PutEntry(ActivosFijos activosFijos)
        {
            AccountEntryDto entryDto = null;
            string json = JsonConvert.SerializeObject(entryDto);
            var httpContent = new StringContent(json, Encoding.UTF8, "application/json");
            var httpResponse = await _httpClient.PutAsync("account-entry", httpContent);
            return httpResponse.StatusCode;
        }

        public AuxiliaryAccountDto GetAuxiliary(int id)
        {
            var response = _httpClient.GetAsync("auxiliary-accounts/active").Result;

            if (response.StatusCode != HttpStatusCode.OK) return null;
            string value = response.Content.ReadAsStringAsync().Result;

            var auxiliariesAccounts = JsonConvert.DeserializeObject<IEnumerable<AuxiliaryAccountDto>>(value);

            var account = auxiliariesAccounts.FirstOrDefault(i => i.Active && i.Id == id);

            return account;
        }

        public CurrencyTypeDto GetCurrencyType(string type)
        {
          
            var response = _httpClient.GetAsync("currency").Result;

            if (response.StatusCode != HttpStatusCode.OK) return null;
            string value = response.Content.ReadAsStringAsync().Result;

            var currencyTypes = JsonConvert.DeserializeObject<IEnumerable<CurrencyTypeDto>>(value);

            var currency = currencyTypes.FirstOrDefault(i => i.Status == 0 && i.Description == type);

            return currency;
        }
    }
}
